# Curso de GraphQL - Apuntes

**Recurso Audio Visual utilizado:** 

[📚 Curso desde cero de GraphQL - Parte 1: ¿Qué es GraphQL? Creando un servidor desde cero con queries](https://www.youtube.com/watch?v=QG-qbmW-wes&list=PL2i4CEznA8jWqp8nsonAjddiN4XctqIuh)


### 1.**¿Que es GraphQL?**

GraphQL es un lenguaje de consultas para APIs de código abierto desarrollado por Facebook, es compatible con muchos lenguajes de programación.

Principales Características:

- **Describe tus Datos:** Debes describir cuales son los datos que quieres recuperar o manipular en tu app.

```graphql
type Project {
	name: String
	tagline: String
	contributors: [User]
}
```

- Puedes Preguntar lo que quieres recuperar:

```graphql
{
	project(name: "GraphQL") {
		tagline
	}
}
```

- Puedes predecir los resultados: Tiene la seguridad de que los datos siempre van a ser de una forma.

```graphql
{
	"project": {
		"tagline": "A query lenguage for APIs"
	}
}
```

Documentación: [GraphQL | A query language for your API](https://graphql.org/)

### 2.¿Porque se desarrolló?

GraphQL se desarrolló con el objetivo de optimizar las peticiones realizadas a las APIs. Facebook en su tiempo, antes del desarrollo utilizaba otros métodos para esto. El problema era que al obtener una lista de post de un usuario también extraían información que no era importante, haciendo que la carga a la hora de hacer peticiones sea mayor.

Con este problema nace GraphQL, ofreciendo la posibilidad de solo extraer los datos necesarios para ciertas tareas y disminuir la carga cuando se hagan las peticiones.

Además de esto, ofrece otras ventajas como la siguiente; pongamos el ejemplo de que quieres extraer los datos de una persona, al igual que sus últimos 5 post, pero tienes la información en dos bases de datos distintas.

Lo que se hace en algunos casos es que se realizan varias llamadas, primero una a la ruta para obtener datos del usuario, y luego otra a la ruta para obtener datos de los post. En GraphQL esto se soluciona ya que puedes crear una solución que retorne estos datos en una sola consulta, el se encargará de buscar la información por nosotros.

### 3.Diferencia entre GraphQL y REST

Son cosas totalmente diferentes, a menudo se hacen la pregunta de que cuales son las diferencias, pero es que no se puede hacer una comparación ya que hablamos de tecnologías totalmente distintas. GraphQL es un lenguaje de consultas para APIs, mientras que REST es una arquitectura para recuperar recursos de las fuentes de datos.

Estas dos tecnologías pueden trabajar entre sí, GraphQL puede consumir APIs REST. Es muy común en un entorno profesional se tengan  muchas APIs desarrolladas, y al implementar GraphQL podemos hacer llamadas a todas estas y de esta forma volvernos totalmente agnósticos de donde están recogiendo estos datos. 

### 4.Creando Servidor GraphQL

Para inicializar el proyecto es necesario tener instalado NodeJS para este ejemplo, luego ubicarse en su entorno de trabajo y ejecutar el siguiente comando: `npm init -y`

Este comando lo que hará es iniciar nuestro archivo `package.json`, el cual contendrá todas las dependencias de nuestra API.

Existen muchas librerías para crear un servidor con GraphQL, pero entre todas estas una de las más famosas  y la que vamos a utilizar es [Apollo Server](https://www.apollographql.com/docs/apollo-server/) (Leer la documentación para mejor entendimiento).


Una vez inicialicemos el proyecto, pasamos a instalar las librerías que estaremos utilizando (Apollo Server y GraphQL.

- `npm install apollo-server graphql`

> Nota: Se recomienda agregar al `package.json` el `“type”: “module”`
> 

### 5.Desarrollando Primera GraphQL API

Para ello estaremos creando un archivo llamado `index.js`, el cual será nuestro archivo principal. A modo de prueba estaremos colocando los datos en el mismo archivo.

**********************************Datos Utilizados:**********************************

```jsx
// Datos de Prueba
const persons = [
  {
    id: 1001,
    name: "Kervin Cruz",
    username: "KervinCruz",
    vida: 15000,
    mana: 5000,
  },
  {
    id: 1002,
    name: "Julio Mercedez",
    username: "Destructor",
    vida: 250000,
    mana: 100000,
  }
];
```

Luego de agregar los datos a nuestro archivo `index.js` continuaremos con la primera de las características mencionadas en la explicación sobre GraphQL, pero antes debemos importar la librería `gql` de `‘apollo-server’`

```jsx
// Importando Librería
import { gql } from 'apollo-server'
```

**Describir los Datos y Consultas:**

```jsx
const typeDefs = gql`
    type Person {
        id: Int!
        name: String!
        username: String!
        vida: Int!
        mana: Int!
        habilidades: [
            {
                tipo: String!
                name: String!
                efecto: Int!
                costo: Int!
            }
        ]
    }

    type Query {
        personCount: Int!
        allPersons: [Person]!
    }
`
```

**Resolviendo las consultas de los Datos:**

```jsx
const resolvers = {
    Query: {
        personCount: () => persons.length,
        allPersons: () => persons
    }
}
```

**Creando e Inicializando Servidor:**

```jsx
// Creando Servidor
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

// Iniciando Servidor
server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
```

Para arrancar el servidor escribir en una terminal `node index.js`. Listo, ya con esto hemos desarrollado la base de nuestro primer servidor con GraphQL.

### 6.Primeras Consultas

Nos dirigimos a la dirección de nuestro servidor (Por defecto `localhost:4000`). Esto te llevará al GraphQL Playground, que es un cliente que levanta el mismo server para hacer pruebas.

**Creación de Petición:**

```graphql
query {
  personCount
  allPersons {
    name
    vida
    mana
  }
}
```

**Respuesta:**

```graphql
{
  "data": {
    "personCount": 2,
    "allPersons": [
      {
        "name": "Kervin Cruz",
        "vida": 15000,
        "mana": 5000
      },
      {
        "name": "Julio Mercedez",
        "vida": 250000,
        "mana": 100000
      }
    ]
  }
}
```


### 7.Mutaciones

Las mutaciones se utilizan para cambiar los datos, como lo puede ser agregar un nuevo objeto a nuestras bases de datos y demás.

Ahora haremos un ejemplo de como agregar un nuevo personaje a nuestra lista de objetos.

**Definiendo nuestras mutaciones:**

```graphql
type Mutation {
    addHabilidad(
        id: Int!
        tipo: String!
        name: String!
        efecto: Int!
    ): Habilidad

    addPerson(
        id: Int!
        name: String!
        username: String!
        vida: Int!
        mana: Int!
        ciudad: String!
        pais: String!
    ): Person
  }
```

**Resolviendo Mutaciones:**

```jsx
  Mutation: {
    // Agregando personas
    addPerson: (root, args) => {
      const person = { ...args };
      person.habilidades = []
      persons.push(person);
      return person;
    },
    // Agregando Habilidades
    addHabilidad: (root, args) => {
      const habilidad = {
        tipo: args.tipo,
        name: args.name,
        efecto: args.efecto,
      };
      persons
        .find((person) => person.id === args.id)
        .habilidades.push(habilidad);

      return habilidad;
    },
  },
```

**Ejecutando Mutaciones:**

```graphql
mutation addPerson {
  addPerson(
    id: 1005, name: "Julio ForySeven", 
    username: "Julio47", vida: 50000, 
    mana: 4560, ciudad: "La Romana", pais: "RepDom") {
    id name
    username vida
    mana address {
      ciudad pais
    }
  }   
}

mutation addHabilidad {
    addHabilidad(id: 1005, tipo: "Daño", name: "Escalibur", efecto: -5000) {
    efecto
    name
    tipo
  }
}
```

### 8.Validar Mutaciones

**Consultar Documentación**: [Error handling - Apollo GraphQL Docs](https://www.apollographql.com/docs/apollo-server/data/errors/)

Para un ejemplo de validaciones estaremos validando si el ID de una persona se repite.

Implementando Validación para Error cometido por el usuario (BAD_USER_INPUT): 

```jsx
// Primero importamos UserInputError
import { ApolloServer, UserInputError, gql } from "apollo-server";

// Realizamos cambios a nuestras mutaciones
  Mutation: {
    addPerson: (root, args) => {
			// Validando
      if (persons.find((p) => p.id === args.id)) {
        throw new UserInputError("Existe un usuario con este ID", {
          invalidArgs: args.id,
        });
      }

      const person = { ...args };
      person.habilidades = [];
      persons.push(person);
      return person;
    },
    // Agregando Habilidades
    addHabilidad: (root, args) => {
      const habilidad = {
        tipo: args.tipo,
        name: args.name,
        efecto: args.efecto,
      };
      persons
        .find((person) => person.id === args.id)
        .habilidades.push(habilidad);

      return habilidad;
    },
  },
```

### 9.ENUMS

Los ENUMS son otra forma de crear datos por así decirlo, para mejor explicación, estaremos haciendo una Query que filtre entre las personas que tienen más de 5000 puntos de mana.

**Definimos nuestro ENUM:**

```jsx
// Dentro de typeDefs
// Creamos nuestro enum
enum YesNo{
	YES
  NO
}
// Lo pasamos como parametro a nuestra consulta
type Query {
	personCount: Int!
  allPersons(mana: YesNo): [Person]!
  findPerson(id: Int!): Person
 }
```

**Resolviendo nuestros ENUMS:**

```jsx
allPersons: (root, args) => {
      // Para saber si se pide o no
      if (!args.mana) return persons;
    
      // Creando nuestro filtro
      return persons.filter((person) => {
        return args.mana === "YES" ? person.mana > 5000 : person.mana <= 5000;
      });

      // Otra forma de hacerlo
      const byMana = (person) =>
        args.mana === "YES" ? person.mana > 5000 : person.mana <= 5000;
      return persons.filter(byMana);
    },
```

**Consulta:**

```graphql
query allPersons {
  allPersons (mana: NO){
    username
    mana
    vida
  }
}
```

### 10.Modificando Datos

Ya con este entendimientos sobre los ENUMS, vamos a desarrollar una mutación que sirva para cambiar el nombre de usuario.

**Definimos nuestra Mutación:**

```jsx
// Desarrollamos nuestra mutación: editUsername
editUsername(
	id: Int!
  username: String!
): Person
```

**Resolvemos nuestra Mutación:**

```jsx
editUsername: (root, args) => {
	const personIndex = persons.findIndex((p) => p.id === args.id);
  if (personIndex === -1) return null;

  const person = persons[personIndex];
  const updatedPerson = { ...person, username: args.username };
  persons[personIndex] = updatedPerson;

  return updatedPerson;
},
```

**Consulta:**

```graphql
mutation editUsername {
  editUsername(id: 1002, username: "Destroller") {
    id username
  }
}
```

### 11.Queries Compuestas

Para obtener la cantidad de personas, los datos de todas las personas que cuentan con mana mayor a 5000 y a su vez a las personas que tienen un mana menor a 5000 en la misma consulta, debemos añadir las consultas como campos, a esto se le conoce como Queries (Consultas) Compuestas.

```graphql
query QueriesCompuestas {
  CantidadPersons: personCount

  PersonsManaYES: allPersons(mana: YES) {
    username mana
  }

  PersonsManaNO: allPersons(mana: NO) {
    username mana
  }
}
```

### 12.Llamadas a REST API

> Para este ejemplo instalar las librerías `json-server` y `axios` con el comando `npm install json-server axios`.
> 

**Creemos nuestra REST API para Pruebas:**

Lo que hacemos es crear un archivo `.json` en nuestro directorio llamado `db.json` y ahí estaremos copiando el arreglo que estábamos utilizando anteriormente como base de datos.

```json
{
  "persons": [
    {
      "id": 1001,
      "name": "Kervin Cruz",
      "username": "KervinCruz",
      "vida": 15000,
      "mana": 5000,
      "ciudad": "La Romana",
      "pais": "RepDom",
      "habilidades": [
        { "tipo": "Salud", "name": "Flor de Loto", "efecto": 10000 },
        { "tipo": "Daño", "name": "Trompada", "efecto": -1000 }
      ]
    },
    {
      "id": 1002,
      "name": "Julio Mercedez",
      "username": "Destructor",
      "vida": 250000,
      "mana": 100000,
      "ciudad": "Santo Domingo",
      "pais": "RepDom",
      "habilidades": [
        { "tipo": "Daño", "name": "Matanza", "efecto": -10000 },
        { "tipo": "Daño", "name": "Trompada", "efecto": -1000 }
      ]
    }
  ]
}
```

Luego, agregamos a nuestro archivo `package.json` el siguiente script marcado en color morado:

```json
{
  "name": "graphql-practicas",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "type": "module",
  "scripts": {
    "json-server": "json-server --watch db.json",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "apollo-server": "^3.12.1",
    "graphql": "^16.8.1",
    "json-server": "^0.17.3"
  }
}
```

Para finalizar este paso, lanzamos nuestra API REST con el siguiente comando `npm run json-server`.

Conectando con API REST

Para esto, importamos AXIOS agregando la siguiente línea al principio del código: `import axios from 'axios';`. Luego de esto, hacemos la siguiente modificación a la parte de allPersons:

```jsx
allPersons: async (root, args) => {
      const { data: personsFromRestAPI } = await axios.get(
        "http://localhost:3000/persons"
      );

      // Para saber si se pide o no
      if (!args.mana) return personsFromRestAPI;

      // Creando nuestro filtro
      return personsFromRestAPI.filter((person) => {
        return args.mana === "YES" ? person.mana > 5000 : person.mana <= 5000;
      });
    },
```

Aquí estamos agregando la ruta de nuestra REST API que generamos en el paso anterior, esta ruta es proporcionada cuando lanzamos el servidor.

> Nota: Para que esto se ejecute, debes asegurarte de que estén corriendo ambos servicios, el servidor de GraphQL y el servidor con tu REST API (Json-Server).
> 

### Conclusión

Se recomienda seguir estudiando la documentación de GraphQL. También puedes realizar pruebas utilizando diferentes APIs REST o GraphQL APIs. Algunas de estas APIs son:

- [The Rick and Morty API](https://rickandmortyapi.com/)
- [api.spacex.land](https://api.spacex.land/)
