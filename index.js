// Importando Librería
import { ApolloServer, UserInputError, gql } from "apollo-server";
import axios from "axios";

// Datos de Prueba
const persons = [
  {
    id: 1001,
    name: "Kervin Cruz",
    username: "KervinCruz",
    vida: 15000,
    mana: 5000,
    ciudad: "La Romana",
    pais: "RepDom",
    habilidades: [
      {
        tipo: "Salud",
        name: "Flor de Loto",
        efecto: 10000,
      },
      {
        tipo: "Daño",
        name: "Trompada",
        efecto: -1000,
      },
    ],
  },
  {
    id: 1002,
    name: "Julio Mercedez",
    username: "Destructor",
    vida: 250000,
    mana: 100000,
    ciudad: "Santo Domingo",
    pais: "RepDom",
    habilidades: [
      {
        tipo: "Daño",
        name: "Matanza",
        efecto: -10000,
      },
      {
        tipo: "Daño",
        name: "Trompada",
        efecto: -1000,
      },
    ],
  },
];

// Describiendo los Datos y Consultas
const typeDefs = gql`
  enum YesNo {
    YES
    NO
  }

  type Direccion {
    ciudad: String!
    pais: String!
  }

  type Habilidad {
    tipo: String!
    name: String!
    efecto: Int!
  }

  type Person {
    id: Int!
    name: String!
    username: String!
    vida: Int!
    mana: Int!
    habilidades: [Habilidad]
    address: Direccion!
  }

  type Query {
    personCount: Int!
    allPersons(mana: YesNo): [Person]!
    findPerson(id: Int!): Person
  }

  type Mutation {
    addHabilidad(
      id: Int!
      tipo: String!
      name: String!
      efecto: Int!
    ): Habilidad

    addPerson(
      id: Int!
      name: String!
      username: String!
      vida: Int!
      mana: Int!
      ciudad: String!
      pais: String!
    ): Person

    editUsername(id: Int!, username: String!): Person
  }
`;

// Resolviendo las consultas de los Datos:
const resolvers = {
  // Resolviendo Consultas
  Query: {
    personCount: () => persons.length,

    allPersons: async (root, args) => {
      const { data: personsFromRestAPI } = await axios.get(
        "http://localhost:3000/persons"
      );

      // Para saber si se pide o no
      if (!args.mana) return personsFromRestAPI;

      // Creando nuestro filtro
      return personsFromRestAPI.filter((person) => {
        return args.mana === "YES" ? person.mana > 5000 : person.mana <= 5000;
      });
    },

    findPerson: (root, args) => {
      const { id } = args;
      console.log(`Datos del usuario: ${id}`);
      return persons.find((person) => person.id === id);
    },
  },

  // Resolviendo nuestras Mutaciones
  Mutation: {
    // Agregando personas
    addPerson: (root, args) => {
      if (persons.find((p) => p.id === args.id)) {
        throw new UserInputError("Existe un usuario con este ID", {
          invalidArgs: args.id,
        });
      }

      const person = { ...args };
      person.habilidades = [];
      persons.push(person);
      return person;
    },
    // Agregando Habilidades
    addHabilidad: (root, args) => {
      const habilidad = {
        tipo: args.tipo,
        name: args.name,
        efecto: args.efecto,
      };
      persons
        .find((person) => person.id === args.id)
        .habilidades.push(habilidad);

      return habilidad;
    },

    editUsername: (root, args) => {
      const personIndex = persons.findIndex((p) => p.id === args.id);
      if (personIndex === -1) return null;

      const person = persons[personIndex];
      const updatedPerson = { ...person, username: args.username };
      persons[personIndex] = updatedPerson;

      return updatedPerson;
    },
  },

  // Resolviendo la direccion y las habilidades de los personajes
  Person: {
    address: (root) => {
      return {
        ciudad: root.ciudad,
        pais: root.pais,
      };
    },
    habilidades: (root) => root.habilidades,
  },
};

// Creando Servidor
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

// Iniciando Servidor
server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
